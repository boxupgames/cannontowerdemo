﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using CryoDI;


public class CameraHandler : CryoBehaviour
{

    [Dependency]
    private GameEvents Events { get; set; }

    void Start()
    {
        Events.OnWindowOrientationChange += OnWindowOrientationChange_Action;
    }

    private void OnWindowOrientationChange_Action(object sender, GameEvents.WindowOrientationChangeEventArgs e)
    {
        Debug.Log("XXXCAM");
        if (e._isLandscape)
        {
            GetComponent<Camera>().fieldOfView = 60;
        } else
        {
            GetComponent<Camera>().fieldOfView = 75;
        }
    }


}
