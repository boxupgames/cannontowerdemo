﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using CryoDI;
using System;
using UnityEngine.UI;

public class UIMessageWelcome : CryoBehaviour
{

    [Dependency]
    private GameEvents Events { get; set; }

    void Start()
    {
        Events.OnLevelLoaded += OnLevelLoaded_Action;
        Events.OnCannonFire += OnCannonFire_Action;
        gameObject.SetActive(false);
    }

    private void OnLevelLoaded_Action(object sender, GameEvents.LevelLoadedEventArgs e)
    {
        gameObject.SetActive(true);
    }

    private void OnCannonFire_Action(object sender, EventArgs e)
    {
        gameObject.SetActive(false);
    }
        
}
