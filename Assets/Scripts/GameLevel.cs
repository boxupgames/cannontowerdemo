﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

public class GameLevel
{

    private int levelNumCurrent;
    private int stageNumCurrent;
    private int figureCount;
    private int figureFallenCount;
    private string levelJSON;
    private Level level;

    public GameLevel(int levelNum)
    {
        levelNumCurrent = levelNum;
        stageNumCurrent = 1;
        figureFallenCount = 0;
        figureCount = 0;
        LoadLevelFromFile();
        CreateLevel();
        CountFigure();
    }

    public LevelEventData CurrentLevelEventData()
    {
        return new LevelEventData
        {
            levelNumCurrent = levelNumCurrent,
            levelNumNext = levelNumCurrent + 1,
            isLevelNextExists = IsLevelFileExists(),
            stageCurrent = stageNumCurrent,
            stageMax = level.stages.Length,
            cannonballCount = levelNumCurrent * 3,
        };
    }

    public bool IsNextLevelNotExist()
    {
        return !IsLevelFileExists();
    }

    public bool IsNextStageLast()
    {
        return (stageNumCurrent + 1) > level.stages.Length;
    }

    public void NextStage()
    {
        figureCount = 0;
        figureFallenCount = 0;
        stageNumCurrent++;
        CountFigure();
    }

    public void NextLevel()
    {
        figureCount = 0;
        figureFallenCount = 0;
        stageNumCurrent = 1;
        levelNumCurrent++;
        LoadLevelFromFile();
        CreateLevel();
        CountFigure();
    }

    public void RestartLevel()
    {
        figureCount = 0;
        figureFallenCount = 0;
        stageNumCurrent = 1;
        CountFigure();
    }

    public Level CurrentLevel()
    {
        return level;
    }

    public int FigureCount()
    {
        return figureCount;
    }

    public void FigureFell()
    {
        figureFallenCount++;
    }

    public int FiguresLeftCount()
    {
        return figureCount - figureFallenCount;
    }

    private void LoadLevelFromFile()
    {
        levelJSON = Resources.Load<TextAsset>("Levels/level-" + levelNumCurrent).ToString();

        /*        string path = Path.Combine(Application.persistentDataPath + "/Resources/Levels/", "level-" + levelNumCurrent + ".json");
                // string path = Application.persistentDataPath + "/Resources/Levels/level-" + levelNumCurrent + ".json";
                if (File.Exists(path))
                {
                    levelJSON = File.ReadAllText(path);
                }
        */
    }

    private bool IsLevelFileExists()
    {
        int nextLevelNum = levelNumCurrent + 1;
        var nextFileJson = Resources.Load<TextAsset>("Levels/level-" + nextLevelNum);
        return (nextFileJson != null);

        /*        int nextLevelNum = levelNumCurrent + 1;
                string path = Application.persistentDataPath + "/Resources/Levels/level-" + nextLevelNum + ".json";
                return File.Exists(path);
        */
    }

    private void CreateLevel()
    {
        level = JsonUtility.FromJson<Level>(levelJSON);
    }

    private void CountFigure()
    {
        figureCount = level.stages[stageNumCurrent - 1].figures.Length;
    }

}

[Serializable]
public class Level
{
    public int levelNum;
    public Stage[] stages;
}

[Serializable]
public class Stage
{
    public Vector3 size;
    public Figure[] figures;
}

[Serializable]
public class Figure
{
    public string type;
    public Vector3 position;
}
