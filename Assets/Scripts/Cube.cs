﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using CryoDI;

public class Cube : CryoBehaviour
{
    [Dependency]
    private GameEvents Events { get; set; }
    
    private bool isFall;

    void Start()
    {
        isFall = false;
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerEnter(Collider other)
    {
        if (!isFall)
        {
            Events.OnCubeDestroy_FireEvent();
            isFall = true;
        }
    }
}
