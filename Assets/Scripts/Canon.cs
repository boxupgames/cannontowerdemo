﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using CryoDI;
using System;

public class Canon : CryoBehaviour
{
    [Dependency]
    private GameEvents Events { get; set; }

    [Dependency("CanonEmitter")]
    private GameObject CanonEmitter { get; set; }

    public GameObject prefab;
    private int cannonballCount = 3;
    private bool isCanShot;

    void Start()
    {
        Events.OnTouchScreen += OnTouchScreen_Action;
        Events.OnLevelLoaded += OnLevelLoaded_Action;
        Events.OnLevelUpdated += OnLevelUpdated_Action;
        Events.OnGameEnd += OnGameEnd_Action;
        
        isCanShot = true;
    }

    private void OnGameEnd_Action(object sender, EventArgs e)
    {
        isCanShot = false;
    }

    private void OnLevelLoaded_Action(object sender, GameEvents.LevelLoadedEventArgs e)
    {
        LevelEventData levelData = e._levelData;
        cannonballCount = levelData.cannonballCount;
        isCanShot = true;
    }

    private void OnLevelUpdated_Action(object sender, GameEvents.LevelLoadedEventArgs e)
    {
        LevelEventData levelData = e._levelData;
        cannonballCount = levelData.cannonballCount;
        isCanShot = true;
    }

    private void OnTouchScreen_Action(object sender, GameEvents.TouchScreenEventArgs e)
    {
        if (cannonballCount > 0 && isCanShot)
            Fire(e._target);
    }

    private void Fire(Vector3 target)
    {
        Renderer r = prefab.GetComponent<Renderer>();
        target += new Vector3(0f, r.bounds.size.y / 2, 0f);

        GameObject ball = Instantiate(prefab, CanonEmitter.transform.position, Quaternion.identity);
        Rigidbody rb = ball.GetComponent<Rigidbody>();

        Vector3 direction = rb.position - target;
        Vector3 force = -direction.normalized * 1000f;
        rb.AddForce(force);

        UpdateCount();
    }

    private void UpdateCount()
    {
        cannonballCount--;
        if (cannonballCount == 0)
        {
            Events.OnCannonFire_FireEvent(cannonballCount);
            StartCoroutine(OnOutOfAmmoDelayTimer());
        } else
        {
            Events.OnCannonFire_FireEvent(cannonballCount);
        }
    }

    IEnumerator OnOutOfAmmoDelayTimer()
    {
        yield return new WaitForSeconds(3f);
        if (cannonballCount == 0)
            Events.OnOutOfAmmo_FireEvent();
    }


}
