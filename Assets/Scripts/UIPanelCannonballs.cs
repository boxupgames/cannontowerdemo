﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using CryoDI;
using System;
using UnityEngine.UI;

public class UIPanelCannonballs : CryoBehaviour
{

    [Dependency]
    private GameEvents Events { get; set; }

    private GameObject UICannonballCount;

    void Start()
    {
        Events.OnCannonFire += OnCannonFire_Action;
        Events.OnOutOfAmmo += OnOutOfAmmo_Action;
        Events.OnLevelLoaded += OnLevelLoaded_Action;
        Events.OnLevelUpdated += OnLevelUpdated_Action;

        UICannonballCount = GameObject.Find("UICannonballCount");
    }

    private void OnLevelLoaded_Action(object sender, GameEvents.LevelLoadedEventArgs e)
    {
        LevelEventData levelData = e._levelData;
        UpdateCanonAmmoCount(levelData.cannonballCount.ToString());
    }

    private void OnLevelUpdated_Action(object sender, GameEvents.LevelLoadedEventArgs e)
    {
        LevelEventData levelData = e._levelData;
        UpdateCanonAmmoCount(levelData.cannonballCount.ToString());
    }

    private void OnCannonFire_Action(object sender, GameEvents.CannonFireEventArgs e)
    {
        UpdateCanonAmmoCount(e._count.ToString());
    }

    private void OnOutOfAmmo_Action(object sender, EventArgs e)
    {
        UpdateCanonAmmoCount("-");
    }

    private void UpdateCanonAmmoCount(string text)
    {
        UICannonballCount.GetComponent<Text>().text = text;
    }


}
