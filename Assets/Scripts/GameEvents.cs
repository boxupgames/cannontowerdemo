﻿using System;
using UnityEngine;

public class GameEvents
{

    public event EventHandler OnGameLaunched;
    public event EventHandler OnGameStart;
    public event EventHandler OnGameEnd;
    public event EventHandler OnLevelRestart;
    public event EventHandler OnStageComplete;
    public event EventHandler OnStageBeforeComplete;
    public event EventHandler OnOutOfAmmo;
    public event CannonFireEventHandler OnCannonFire;
    
    public event LevelLoadedEventHandler OnLevelLoaded;
    public event LevelLoadedEventHandler OnLevelUpdated;
    public event EventHandler OnCubeDestroy;

    public event TouchScreenEventHandler OnTouchScreen;

    public event OnWindowOrientationChangeEventHandler OnWindowOrientationChange;


    public delegate void OnWindowOrientationChangeEventHandler(object sender, WindowOrientationChangeEventArgs e);
    public void OnWindowOrientationChange_FireEvent(bool isLandscape)
    {
        WindowOrientationChangeEventArgs args = new WindowOrientationChangeEventArgs(isLandscape);
        OnWindowOrientationChange?.Invoke(this, args);
    }
    public class WindowOrientationChangeEventArgs : EventArgs
    {
        public bool _isLandscape { get; set; }
        public WindowOrientationChangeEventArgs(bool isLandscape)
        {
            _isLandscape = isLandscape;
        }
    }


    public delegate void CannonFireEventHandler(object sender, CannonFireEventArgs e);
    public void OnCannonFire_FireEvent(int count)
    {
        CannonFireEventArgs args = new CannonFireEventArgs(count);
        OnCannonFire?.Invoke(this, args);
    }
    public class CannonFireEventArgs : EventArgs
    {
        public int _count { get; set; }
        public CannonFireEventArgs(int count)
        {
            _count = count;
        }
    }


    public delegate void TouchScreenEventHandler(object sender, TouchScreenEventArgs e);
    public void OnTouchScreen_FireEvent(Vector3 target)
    {
        TouchScreenEventArgs args = new TouchScreenEventArgs(target);
        OnTouchScreen?.Invoke(this, args);
    }
    public class TouchScreenEventArgs : EventArgs
    {
        public Vector3 _target { get; set; }
        public TouchScreenEventArgs(Vector3 target)
        {
            _target = target;
        }
    }


    public void OnLevelUpdated_FireEvent(LevelEventData levelData)
    {
        LevelLoadedEventArgs args = new LevelLoadedEventArgs(levelData);
        OnLevelUpdated?.Invoke(this, args);
    }

    public delegate void LevelLoadedEventHandler(object sender, LevelLoadedEventArgs e);
    public void OnLevelLoaded_FireEvent(LevelEventData levelData)
    {
        LevelLoadedEventArgs args = new LevelLoadedEventArgs(levelData);
        OnLevelLoaded?.Invoke(this, args);
    }
    public class LevelLoadedEventArgs : EventArgs
    {
        public LevelEventData _levelData { get; set; }
        public LevelLoadedEventArgs(LevelEventData levelData)
        {
            _levelData = levelData;
        }
    }

    public void OnCubeDestroy_FireEvent()
    {
        OnCubeDestroy?.Invoke(this, EventArgs.Empty);
    }

    public void OnGameLaunched_FireEvent()
    {
        OnGameLaunched?.Invoke(this, EventArgs.Empty);
    }

    public void OnGameStart_FireEvent()
    {
        OnGameStart?.Invoke(this, EventArgs.Empty);
    }

    public void OnStageBeforeComplete_FireEvent()
    {
        OnStageBeforeComplete?.Invoke(this, EventArgs.Empty);
    }

    public void OnStageComplete_FireEvent()
    {
        OnStageComplete?.Invoke(this, EventArgs.Empty);
    }

    public void OnOutOfAmmo_FireEvent()
    {
        OnOutOfAmmo?.Invoke(this, EventArgs.Empty);
    }

    public void OnLevelRestart_FireEvent()
    {
        OnLevelRestart?.Invoke(this, EventArgs.Empty);
    }

    public void OnGameEnd_FireEvent()
    {
        OnGameEnd?.Invoke(this, EventArgs.Empty);
    }

}
