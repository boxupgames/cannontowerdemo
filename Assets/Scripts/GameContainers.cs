﻿using UnityEngine;
using CryoDI;

public class GameContainers : UnityStarter
{

    protected override void SetupContainer(CryoContainer container)
    {
        container.RegisterSingleton<GameEvents>(LifeTime.Global);

        container.RegisterSceneObject<GameObject>("Throne/ThroneBase", "Throne", LifeTime.Scene);
        container.RegisterSceneObject<GameObject>("Figures", "FiguresContainer", LifeTime.Scene);
        container.RegisterSceneObject<GameObject>("Canon/CanonEmitter", "CanonEmitter", LifeTime.Scene);
    }

}
