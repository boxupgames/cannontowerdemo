﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using CryoDI;
using System;

public class Game : CryoBehaviour
{
    [Dependency]
    private GameEvents Events { get; set; }

    [Dependency("FiguresContainer")]
    private GameObject FiguresContainer { get; set; }

    public GameObject cube;
    
    private GameObject throneBase;
    private GameLevel gameLevel;

    void Start()
    {
        Events.OnGameStart += OnGameStart_Action;
        Events.OnCubeDestroy += OnCubeDestroy_Action;
        Events.OnStageComplete += OnStageComplete_Action;
        Events.OnLevelRestart += OnLevelRestart_Action;
        Events.OnGameEnd += OnGameEnd_Action;

        StartTheGame();
    }

    private void OnStageComplete_Action(object sender, EventArgs e)
    {
        if (IsNextStageLast())
        {
            if (IsNextLevelNotExist()) 
            {
                Events.OnGameEnd_FireEvent();
                return;
            }
            NextLevel();
        }
        else
        {
            NextStage();
        }
        ResizeThroneBase();
        BuildFigures();
        Events.OnLevelUpdated_FireEvent(gameLevel.CurrentLevelEventData());
    }

    private void OnLevelRestart_Action(object sender, EventArgs e)
    {
        gameLevel.RestartLevel();
        ResizeThroneBase();
        BuildFigures();
        Events.OnLevelUpdated_FireEvent(gameLevel.CurrentLevelEventData());
    }

    private void OnGameEnd_Action(object sender, EventArgs e)
    {
        Debug.Log("GAME ENDED");
    }

    private bool IsNextLevelNotExist()
    {
        return gameLevel.IsNextLevelNotExist();
    }

    private bool IsNextStageLast()
    {
        return gameLevel.IsNextStageLast();
    }

    private void NextStage()
    {
        gameLevel.NextStage();
    }

    private void NextLevel()
    {
        gameLevel.NextLevel();
    }

    private void StartTheGame()
    {
        CreateLevel();
        ResizeThroneBase();
        BuildFigures();
    }
    private void OnGameStart_Action(object sender, EventArgs e)
    {
        StartTheGame();
    }

    private void OnCubeDestroy_Action(object sender, EventArgs e)
    {
        gameLevel.FigureFell();
        if (gameLevel.FiguresLeftCount() <= 0)
        {
            Events.OnStageBeforeComplete_FireEvent();
            StartCoroutine(OnStageCompleteDelayTimer());
        }
    }

    IEnumerator OnStageCompleteDelayTimer()
    {
        yield return new WaitForSeconds(1f);
        Events.OnStageComplete_FireEvent();
    }

    private void CreateLevel()
    {
        gameLevel = new GameLevel(1);
        StartCoroutine(OnCreateFirstLevelDelayTimer());
    }

    IEnumerator OnCreateFirstLevelDelayTimer()
    {
        yield return new WaitForSeconds(0f);
        Events.OnLevelLoaded_FireEvent(gameLevel.CurrentLevelEventData());
    }


    private void ResizeThroneBase()
    {
        throneBase = GameObject.Find("Throne/ThroneBase");
        throneBase.transform.localScale = gameLevel.CurrentLevel().stages[gameLevel.CurrentLevelEventData().stageCurrent - 1].size;
    }

    private void BuildFigures()
    {

        foreach (Transform child in FiguresContainer.transform)
        {
            Destroy(child.gameObject);
        }

        LevelBuilder levelbuilder = GameObject.Find("LevelBuilder")?.GetComponent<LevelBuilder>();
        if (levelbuilder == null )
        {
            levelbuilder = new GameObject().AddComponent<LevelBuilder>();
            levelbuilder.name = "LevelBuilder";
        }
        levelbuilder.BuildFigures(gameLevel.CurrentLevel().stages[gameLevel.CurrentLevelEventData().stageCurrent - 1], cube);
    }




}
