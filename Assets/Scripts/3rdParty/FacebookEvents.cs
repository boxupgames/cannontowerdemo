﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using CryoDI;
using Facebook.Unity;

public class FacebookEvents : CryoBehaviour
{

    [Dependency]
    private GameEvents Events { get; set; }

    void Start()
    {
        Events.OnLevelUpdated += OnLevelUpdated_Action;
    }

    private void OnLevelUpdated_Action(object sender, GameEvents.LevelLoadedEventArgs e)
    {
        Dictionary<string, object> levelComplete = new Dictionary<string, object>();
        levelComplete[AppEventParameterName.ContentID] = "level_complete_" + e._levelData.levelNumCurrent;
        levelComplete[AppEventParameterName.Description] = "Level " + e._levelData.levelNumCurrent + " complete.";
        levelComplete[AppEventParameterName.Success] = "1";

        FB.LogAppEvent(
            AppEventName.AchievedLevel,
            parameters: levelComplete
        );
    }


}
