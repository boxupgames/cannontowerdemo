﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using CryoDI;
using System;

public class UIRestartLevel : CryoBehaviour
{

    [Dependency]
    private GameEvents Events { get; set; }


    void Start()
    {
        Events.OnOutOfAmmo += OnOutOfAmmo_Action;
        Events.OnLevelUpdated += OnLevelUpdated_Action;
        gameObject.SetActive(false);
    }

    private void OnOutOfAmmo_Action(object sender, EventArgs e)
    {
        gameObject.SetActive(true);
    }

    private void OnLevelUpdated_Action(object sender, EventArgs e)
    {
        gameObject.SetActive(false);
    }

    public void OnClick()
    {
        Events.OnLevelRestart_FireEvent();
        gameObject.SetActive(false);
    }

}
