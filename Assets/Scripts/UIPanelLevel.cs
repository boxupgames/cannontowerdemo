﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using CryoDI;
using System;

public class UIPanelLevel : CryoBehaviour
{
    [Dependency]
    private GameEvents Events { get; set; }

    public GameObject stagePrefab;

    private GameObject UILevelCurrent;
    private GameObject UILevelNext;
    private GameObject UIStagesContainer;

    void Start()
    {
        Events.OnLevelLoaded += OnLevelLoaded_Action;
        Events.OnLevelUpdated += OnLevelUpdated_Action;

        UILevelCurrent = GameObject.Find("UILevelCurrent");
        UILevelNext = GameObject.Find("UILevelNext");
        UIStagesContainer = GameObject.Find("UIStagesContainer");
    }

    private void OnLevelLoaded_Action(object sender, GameEvents.LevelLoadedEventArgs e)
    {
        LevelEventData levelData = e._levelData;
        FillLevels(levelData.levelNumCurrent, levelData.levelNumNext, !levelData.isLevelNextExists);
        FillStages(levelData.stageCurrent, levelData.stageMax);
    }

    private void OnLevelUpdated_Action(object sender, GameEvents.LevelLoadedEventArgs e)
    {
        LevelEventData levelData = e._levelData;
        FillLevels(levelData.levelNumCurrent, levelData.levelNumNext, !levelData.isLevelNextExists);
        FillStages(levelData.stageCurrent, levelData.stageMax);
    }

    private void FillLevels(int current, int next, bool isLast = false)
    {
        UILevelCurrent.GetComponent<Text>().text = current.ToString();

        UILevelNext.GetComponent<Text>().text = (isLast) ? "Ф" : next.ToString();
    }

    private void FillStages(int current, int max)
    {
        RectTransform containerRT = UIStagesContainer.transform.GetComponent<RectTransform>();
        float containerWidth = containerRT.rect.width;

        RectTransform stagePrefabRT = stagePrefab.transform.GetComponent<RectTransform>();
        float stageObjectWidth = containerWidth / max;

        foreach (Transform child in UIStagesContainer.transform)
        {
            Destroy(child.gameObject);
        }

        for (int i = 1; i <= max; i++)
        {
            GameObject s = Instantiate(stagePrefab, containerRT.transform.position, Quaternion.identity, UIStagesContainer.transform);
            RectTransform sRT = s.transform.GetComponent<RectTransform>();
            sRT.sizeDelta = new Vector2(stageObjectWidth, sRT.sizeDelta.y);
            sRT.localPosition = new Vector3((i - 1) * stageObjectWidth, 0f);
            if (i < current)
            {
                Color colorPassed = new Color(.1f, .7f, .3f);
                s.transform.GetComponent<Image>().color = colorPassed;
            }
            if (i == current)
            {
                Color colorSelected = new Color(.5f, .3f, .4f);
                s.transform.GetComponent<Image>().color = colorSelected;
            }
        }
    }

}
