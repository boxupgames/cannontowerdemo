﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using CryoDI;

public class ScreenInput : CryoBehaviour
{
    [Dependency]
    private GameEvents Events { get; set; }

    void Update()
    {
        if (Input.touchCount > 0)
        {
            Touch touch = Input.GetTouch(0);

            Debug.Log(touch.phase);

        }

        if (Input.GetMouseButtonDown(0))
        {
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            RaycastHit hit;
            Vector3 target = new Vector3();

            if (Physics.Raycast(ray, out hit, 20f, LayerMask.GetMask("TouchPad")))
            {
                if (hit.collider != null && hit.collider.gameObject.name == "TouchPad")
                {
                    target = hit.point;
                    Events.OnTouchScreen_FireEvent(target);
                }
            } 
        }
    }

}
