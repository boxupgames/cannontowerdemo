﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using CryoDI;
using System;

public class Cannonball : CryoBehaviour
{

    [Dependency]
    private GameEvents Events { get; set; }

    public float lifeTimeMax = 10f;
    private float lifeTimeCurrent;
    private Rigidbody rb;
    private TrailRenderer trail;

    void Start()
    {
        Events.OnStageComplete += OnStageComplete_Action;
        Events.OnLevelRestart += OnStageComplete_Action;

        lifeTimeCurrent = 0f;
        rb = gameObject.GetComponent<Rigidbody>();
        trail = gameObject.GetComponent<TrailRenderer>();
    }

    void Update()
    {
        SwitchTail();
        UpdateLifeTime();
        CheckDeath();
    }

    private void OnStageComplete_Action(object sender, EventArgs e)
    {
        Destroy(gameObject);
    }

    void OnDestroy()
    {
        Events.OnStageComplete -= OnStageComplete_Action;
        Events.OnLevelRestart -= OnStageComplete_Action;
    }

    private void SwitchTail()
    {
        trail.time = (rb.velocity.magnitude > 5f) ? 1f : 0f;
    }

    private void UpdateLifeTime()
    {
        lifeTimeCurrent += Time.deltaTime;
    }

    private void CheckDeath()
    {
        if (IsDeath())
        {
            Destroy(gameObject);
        }
    }

    private bool IsDeath()
    {
        if (lifeTimeCurrent >= lifeTimeMax) return true;
        if (gameObject.transform.position.y < -20f) return true;
        return false;
    }

}
