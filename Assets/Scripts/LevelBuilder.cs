﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using CryoDI;

public class LevelBuilder : CryoBehaviour
{

    [Dependency("FiguresContainer")]
    private GameObject FiguresContainer { get; set; }

    [Dependency("Throne")]
    private GameObject Throne { get; set; }

    private Stage stage;
    private GameObject prefab;

    public void BuildFigures(Stage stage, GameObject prefab)
    {
        this.prefab = prefab;
        this.stage = stage;

        foreach (Figure figure in this.stage.figures)
        {
            InstantiateFigure(figure);
        }
    }

    private void InstantiateFigure(Figure figure)
    {
        Vector3 offset = new Vector3(FigureOffset.GetOffsetX((int)stage.size.x) + stage.size.x % 2, Throne.transform.localScale.y / 2);
        GameObject f = Instantiate(prefab, GetTrhronePosition() + figure.position - offset, Quaternion.identity, FiguresContainer.transform);
        Color randomColor = new Color(Random.Range(0f, 1f), Random.Range(0f, 1f), Random.Range(0f, 1f));
        f.GetComponent<MeshRenderer>().material.color = randomColor;
    }

    private Vector3 GetTrhronePosition()
    {
        return Throne.transform.position;
    }

}

public class FigureOffset
{

    public static float GetOffsetX(int x)
    {
        switch (x)
        {
            case 1:
                return 0f;
            case 2:
                return 1f;
            case 3:
                return 1f;
            case 4:
                return 2.5f;
            case 5:
                return 2f;
            case 6:
                return 3.5f;
            case 7:
                return 3f;
            case 8:
                return 4.5f;
            case 9:
                return 4f;
            case 10:
                return 5.5f;
            default:
                return 0f;
        }
    }

}