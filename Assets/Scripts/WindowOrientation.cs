﻿using UnityEngine;
using CryoDI;
using System;

public class WindowOrientation : CryoBehaviour
{

    private float LastScreenWidth;
    private float LastScreenHeight;
    private RectTransform RectTransform;
    private bool IsOrientationLandscape;

    [Dependency]
    private GameEvents Events { get; set; }

    void Start()
    {
        IsOrientationLandscape = true; // Default orientation
        RectTransform = GetComponent<RectTransform>();
        UpdateOrientation();
    }

    void Update()
    {
        if (LastScreenWidth != Screen.width || LastScreenHeight != Screen.height)
        {
            LastScreenWidth = Screen.width;
            UpdateScreenSize();
        }
    }

    public void UpdateOrientation()
    {
        if (RectTransform == null) return;
        bool IsWidthMoreHeight = Screen.width >= Screen.height ? true : false;
        if (IsOrientationLandscape != IsWidthMoreHeight)
        {
            IsOrientationLandscape = IsWidthMoreHeight;
            Events.OnWindowOrientationChange_FireEvent(IsOrientationLandscape);
        }
    }

    public void OnRectTransformDimensionsChange()
    {
        UpdateOrientation();
    }

    public void UpdateScreenSize()
    {
        LastScreenWidth = Screen.width;
        LastScreenHeight = Screen.height;
    }

}
