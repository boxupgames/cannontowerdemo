﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using CryoDI;
using System;

public class UIRestartGame : CryoBehaviour
{

    [Dependency]
    private GameEvents Events { get; set; }


    void Start()
    {
        Events.OnGameEnd += OnGameEnd_Action;

        gameObject.SetActive(false);
    }

    private void OnGameEnd_Action(object sender, EventArgs e)
    {
        gameObject.SetActive(true);
    }

    public void OnClick()
    {
        Events.OnGameStart_FireEvent();
        gameObject.SetActive(false);
    }


}
