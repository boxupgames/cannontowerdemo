﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using CryoDI;
using System;
using UnityEngine.UI;

public class UIMessageSuccess : CryoBehaviour
{

    [Dependency]
    private GameEvents Events { get; set; }


    void Start()
    {
        Events.OnStageBeforeComplete += OnStageBeforeComplete_Action;
        Events.OnLevelUpdated += OnLevelUpdated_Action;
        Events.OnGameEnd += OnGameEnd_Action;
        gameObject.SetActive(false);
    }

    private void OnStageBeforeComplete_Action(object sender, EventArgs e)
    {
        gameObject.GetComponent<Text>().text = "Успех!";
        gameObject.SetActive(true);
    }

    private void OnGameEnd_Action(object sender, EventArgs e)
    {
        gameObject.GetComponent<Text>().text = "Победа! Игра окончена.";
        gameObject.SetActive(true);
    }

    private void OnLevelUpdated_Action(object sender, EventArgs e)
    {
        gameObject.SetActive(false);
    }

}
