﻿public class LevelEventData
{
    public int levelNumCurrent;
    public int levelNumNext;
    public bool isLevelNextExists;
    public int stageCurrent;
    public int stageMax;
    public int cannonballCount;
}
